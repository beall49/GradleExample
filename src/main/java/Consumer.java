import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import rx.functions.Action0;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

import java.util.List;


class Consumer {
	private List<Obj> objects;

	Consumer() {
	}

	void callAnApi() {
		Endpoints api = new Retrofit.Builder()
				.baseUrl("https://g3enterprises-test.cloudhub.io/api/")
				.addConverterFactory(GsonConverterFactory.create())
				.addCallAdapterFactory(RxJavaCallAdapterFactory.create())
				.build()
				.create(Endpoints.class);


		api.getTests()
		   .observeOn(Schedulers.io())
		   .subscribe(onNextAction, onErrorAction, onCompleteAction);
		new APIObject().sortObjects();
	}

	private Action0 onCompleteAction = () -> System.out.println("");

	private Action1<Throwable> onErrorAction = e -> e.printStackTrace();

	private Action1<List<Obj>> onNextAction = obj -> {
		ObjList.getInstance().setObjects(obj);
		objects = ObjList.getInstance().getObjects();
	};

}