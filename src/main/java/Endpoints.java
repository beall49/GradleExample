import retrofit2.http.GET;
import rx.Observable;
import java.util.List;


interface Endpoints {
	@GET("entries/15") Observable<List<Obj>> getEntries();

	@GET("tests") Observable<List<Obj>> getTests();
}

