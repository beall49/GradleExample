import com.google.gson.annotations.SerializedName;

class Obj {
    @SerializedName("name") private String name;
    @SerializedName("year") private String year;

    private Obj() {
    }

    String getName() {
        return name;
    }

    void setName(String _name) {
        this.name = _name;
    }

    int getYear() {
        return Integer.parseInt(year);
    }
}


