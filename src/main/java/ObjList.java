import java.util.ArrayList;
import java.util.List;

public class ObjList {
    private static final ObjList INSTANCE = new ObjList();

    public static ObjList getInstance() {
        return INSTANCE;
    }

    private ObjList() {
    }

    private List<Obj> objects = new ArrayList<>();

    public void setObjects(List<Obj> objects){
        this.objects=objects;
    }

    public List<Obj> getObjects(){
        return this.objects;
    }

}

