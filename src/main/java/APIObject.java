import java.util.List;

class APIObject {
	private final List<Obj> objects;


	APIObject() {
		objects = ObjList.getInstance().getObjects();
	}

	/*
		these are all chained events
		first line takes an object and turns it into a stream
		then a one line lambda sort
		the forEach loops through the object only AFTER
		the sort above is completely finished
	*/
	void sortObjects() {
		objects.stream()
			   .sorted((obj1, obj2) -> Integer.compare(obj1.getYear(), obj2.getYear()))
			   .forEach(obj -> System.out.printf("Name= %s - Year Started= %s\n", obj.getName(), obj.getYear()));
	}

	/*
		changes a value based on another value
		people are apparently using this
		to implement sql type filtering/querying
		on objects in java
	*/
	void filterObjects() {
		objects.stream()
			   .filter(obj -> obj.getName().equals("Ryan"))
			   .forEach(obj -> obj.setName("Guy in the corner"));
	}


}
